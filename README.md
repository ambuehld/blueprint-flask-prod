# Microservices - Produktionsumgebung

## Technologien

- Flask (https://flask.palletsprojects.com/en/3.0.x/)
- ApiFlask (https://apiflask.com/)
- SQLAlchemy (https://www.sqlalchemy.org/)
- mySQL (https://www.mysql.com/de/)
- Docker Compose (https://docs.docker.com/compose/)
- pyTest (https://docs.pytest.org/en/8.0.x/)
- Gunicorn (https://gunicorn.org/)

## Zweck

Dies ist eine Beispielapplikation für einen dockerized Flask-Server mit einer Dev und einer Prod Variante.

Es sollen Konzepte demonstriert werden, mit denen man von einer Entwicklungsumgebung einfach auf eine Produktionsumgebung wechseln kann.

Der Source Code ist ausschliesslich für Entwicklungszwecke gedacht.


## Funktionen

- Studenten und Kurse mit CRUD verwalten
- Studenten auf Kurse registrieren
- Kurse mit Studenten verknüpfen
- Alle Studenten eines Kurses anzeigen
- Alle Kurse eines Studenten anzeigen

## Installation

klone dieses Repo und wechsle in das Verzeichnis mit der Datei compose.yaml

Development Env (mit Hot Reload):

```bash
docker compose up --build
```

Tests ausführen:

```bash
docker compose -f compose.test.yaml up --build
```

Version Produktion:

```bash
docker compose -f compose.prod.yaml up --build
```

# Elastic IP Adress

3.212.31.242

# Einrichten

ssh -i "flask-prod.pem" ec2-user@3.212.31.242
cd blueprint-flask-prod
ec2-3-212-31-242.compute-1.amazonaws.com

http://ec2-3-212-31-242.compute-1.amazonaws.com/docs

# Git Configurierne

ssh-keygen -t ed25519 -C ambuehld@gmail.com
git config --global user.email ambuehld@gmail.com
git config --global user.name @ambuehld
git config --global --list
PUBLIC KEY EINTRAGEN IN GITLAB
git clone git@gitlab.com:ambuehld/blueprint-flask-prod.git    -> Did not work
git clone https://gitlab.com/ambuehld/blueprint-flask-prod.git -> did work
docker compose -f compose.prod.yaml up --build

# Mermaid

```mermaid
---
title: GitLab Flow Diagram
---
gitGraph
   commit
   commit
   branch feature
   checkout feature
   commit
   commit
   checkout main
   merge feature
   commit
```

# Pipeline

Resources: https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/C_Towards_Production/05_automation.md?ref_type=heads

```yaml
sudo ssh-keygen -l -f ~/.ssh/authorized_keys
cat .ssh/authorized_keys
-> Gitlab SSH_HOST_KEY eintragen 

cat flask-prod.pem
-> Gitlab SSH_PRIVATE_KEY eintragen

-> Gitlab DEPLOY_TARGET ec2-3-212-31-242.compute-1.amazonaws.com

```

# Debuging 

Testing Localy
```bash
docker compose up --build
docker compose -f compose.test.yaml up --build 
```

git remote set-url origin https://oauth2:<your_token>@gitlab.com/ambuehld/blueprint-flask-prod.git

## Lizenz

© 2024. This work is openly licensed via [CC BY-NC.SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
