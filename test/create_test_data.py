
from app.extensions import db
from app.models.student import Student
from app.models.course import Course
from app.models.users import Users

# Hilfsfunktion (Testdaten erstellen, Tabellen erstellen)
def create_test_data():
    db.drop_all() # dieser Befehl löscht alle vorhandenen Datenbankeintraege und Tabellen
    db.create_all()

    # Beispieldaten
    students = [
        {'name': 'Freda Kids', 'level': 'HF'},
        {'name': 'Sam Sung', 'level': 'HF'},
        {'name': 'Chris P. Bacon', 'level': 'AP'},
        {'name': 'Saad Maan', 'level': 'PE'}
    ]
    for student_data in students:
        student = Student(**student_data)
        db.session.add(student)

    # create an additional student and safe ref for later
    student_ref = Student(name='Mega Tron', level='HF')
    db.session.add(student_ref)

    # Courses Testdaten
    courses = [
        {'title': 'M231 Security'},
        {'title': 'M254 BPMN'}
    ]
    for course_data in courses:
        course = Course(**course_data)
        db.session.add(course)

    # create an additional course and safe ref for later
    course_ref = Course(title='M347 Kubernetes')
    db.session.add(course_ref)
    student_ref.courses.append(course_ref)

    # Users Testdaten
    users = [
        {'email': 'fabio@master.ch', 'name': 'Fabio', 'password': '12345'},
        {'email': 'Roger@swisscom.ch', 'name': 'Roger', 'password': '12345'}
    ]
    for user_data in users:
        user = Users(**user_data)
        db.session.add(user)

    # create an additional course and safe ref for later
    user_ref = Users(email='master@swisscom.com', name='Mega Tron', password='12345')
    db.session.add(user_ref)

    db.session.commit()