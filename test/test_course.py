from test import client
from test import create_test_data

def test_get_course(client):
    response = client.get("/courses/1")
    assert response.json['title'] == 'M231 Security'

def test_post_course(client):
    response = client.post("/courses/", json={
            'title': 'FAAS2'
        })
    assert response.status_code == 201

