from test import client
from test import create_test_data

def test_get_token(client):
    response0 = client.post("/users/login", json={'email': 'fabio@master.ch', 'name': 'Fabio', 'password': '12345'})
    access_token = response0.json["token"]
    headers = { 'Authorization': 'Bearer {}'.format(access_token) }
    assert headers['Authorization'].startswith('Bearer')
    return headers

def test_get_users(client):
    response = client.get("/users/")
    assert response.json[1]['name'] == 'Roger'

def test_get_user(client):
    response = client.get("/users/1")
    assert response.json["name"] == "Fabio"

def test_post_user(client):
    headers = test_get_token(client)
    
    response = client.post("/users/", json={
            'name': 'Nina Hagen', 'email': 'AP@Hagen.ch', 'password': 'Password'
        }, headers=headers)
    assert response.status_code == 201

def test_delete_user(client):
    headers = test_get_token(client)
    response = client.delete("/users/1", headers=headers)

    assert response.status_code == 204

