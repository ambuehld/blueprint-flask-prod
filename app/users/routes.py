from app.users import bp
from app.extensions import db, check_password_hash
from app.models.users import Users, UsersIn, UsersOut, LoginIn, TokenOut
from app.auth import token_auth
import logging
logging.basicConfig(level=logging.INFO)

####
## view functions
####    
@bp.get('/<int:users_id>')
@bp.output(UsersOut)
def get_user(users_id):
    return db.get_or_404(Users, users_id)

@bp.get('/')
@bp.output(UsersOut(many=True))
def get_users():
    return Users.query.all()

@bp.post('/')
@bp.input(UsersIn, location='json')
@bp.auth_required(token_auth)
@bp.output(UsersOut, status_code=201)
def create_users(json_data):
    users = Users(**json_data)
    db.session.add(users)
    db.session.commit()
    return users

@bp.delete('/<int:users_id>')
@bp.auth_required(token_auth)
@bp.output({}, status_code=204)
def delete_user(users_id):
    user = db.get_or_404(Users, users_id)
    db.session.delete(user)
    db.session.commit()
    return ''

@bp.post('/login')
@bp.input(LoginIn, location='json')
@bp.output(TokenOut, status_code=200)
def login_user(json_data):
    # Find user by email
    user = Users.query.filter_by(email=json_data.get('email')).first()
    # If user doesn't exist or password is wrong
    if not user or not user.check_password(json_data.get('password')):
        logging.warning(f"LOG: Failed login attempt for email: {json_data.get('email')}")
        return {'message': 'Invalid email or password'}, 401
    
    token = user.generate_auth_token(600)
    logging.info(f"LOG: User logged in with email: {json_data.get('email')}")
    return { 'token': token, 'duration': 600 }
