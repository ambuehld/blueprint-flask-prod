from apiflask import APIFlask

from config import Config
from app.extensions import db

# Dashboard
import flask_monitoringdashboard as dashboard


def create_app(config_class=Config):
    app = APIFlask(__name__)
    
    app.config.from_object(config_class)

    # Initialize Flask extensions here
    db.init_app(app)
        
    # Register blueprints here
    from app.students import bp as students_bp
    app.register_blueprint(students_bp, url_prefix='/students')

    from app.courses import bp as courses_bp
    app.register_blueprint(courses_bp, url_prefix='/courses')

    from app.users import bp as users_bp
    app.register_blueprint(users_bp, url_prefix='/users')

    with app.app_context():
        db.create_all()

    @app.route('/')
    def test_page():
        return {'message': 'Testing the Flask Application Factory Pattern Feauture BRANCH'}

    # Dashboard
    if app.config['DASHBOARD_ENABLE']:
        dashboard.config.database_name = 'sqlite:////var/data/monitoring.db'
        dashboard.bind(app)

    return app